using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Homework.UnitTests
{
    [TestFixture]
    public class AccountTest
    {
        Account account;

        Mock<IAction> _mockAction = new Mock<IAction>();

        [SetUp]
        public void SetUp()
        {
            account = new Account(1);
        }

        [Test]
        public void Constructor_FieldsCorrect()
        {
            //ASSERT
            Assert.That(account.Id, Is.EqualTo(1));
            Assert.That(account.IsRegistered, Is.False);
            Assert.That(account.IsConfirmed, Is.False);
            Assert.That(account.ActionsSuccessfullyPerformed, Is.EqualTo(0));
        }

        [Test]
        public void GivenExistingAccount_Register_GetsRegistered()
        {
            //ACT
            account.Register();
            //ASSERT
            Assert.That(account.IsRegistered, Is.True);
        }

        [Test]
        public void GivenExistingAccount_Activate_GetsConfirmed()
        {
            //ACT
            account.Activate();
            //ASSERT
            Assert.That(account.IsConfirmed, Is.True);
        }

        [Test]
        public void GivenExistingValidAccount_TakeAction_Successful()
        {
            //ARRANGE
            account.Activate();
            account.Register();

            _mockAction.Setup(action => action.Execute()).Returns(true);

            //ACT
            bool success = account.TakeAction(_mockAction.Object);
            //ASSERT
            Assert.That(success, Is.True);
        }

        [Test]
        public void GivenExistingInvalidAccount_TakeAction_ThrowsException()
        {
            //ASSERT
            Assert.That(() => account.TakeAction(_mockAction.Object), Throws.Exception.TypeOf<InactiveUserException>());
        }
    }
}
