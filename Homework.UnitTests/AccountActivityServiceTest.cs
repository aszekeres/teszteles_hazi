using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System;

namespace Homework.UnitTests
{

    [TestFixture]
    public class AccountActivityServiceTest
    {
        Mock<IAction> _mockAction = new Mock<IAction>();

        Mock<IAccountRepository> _mockAccountRepository;

        AccountActivityService service;

        [SetUp]
        public void SetUp()
        {
            _mockAccountRepository = new Mock<IAccountRepository>();

            service = new AccountActivityService(_mockAccountRepository.Object);
        }

        [Test]
        public void GivenNonExistentAccount_GetActivity_ThrowsException()
        {
            Assert.That(() => service.GetActivity(1), Throws.Exception.TypeOf<AccountNotExistsException>());
        }

        [TestCase(0, ExpectedResult = ActivityLevel.None)]
        [TestCase(19, ExpectedResult = ActivityLevel.Low)]
        [TestCase(39, ExpectedResult = ActivityLevel.Medium)]
        [TestCase(40, ExpectedResult = ActivityLevel.High)]
        public ActivityLevel GivenExistentAccount_GetActivity_ReturnsActivityLevel(int actionCount)
        {
            //ARRANGE
            _mockAction.Setup(action => action.Execute()).Returns(true);

            Account account = new Account(1);
            account.Activate();
            account.Register();

            for (int i = 0; i < actionCount; i++)
            {
                account.TakeAction(_mockAction.Object);
            }
            _mockAccountRepository.Setup(repo => repo.Get(1)).Returns(account);
            //ACT
            ActivityLevel activity = service.GetActivity(1);

            return activity;
        }


        [TestCase(0, ActivityLevel.None, ExpectedResult = 1)]
        public int GetAmountForActivity_ReturnsAmount(int actionCount, ActivityLevel activityLevel)
        {
            //ARRANGE
            Account account = new Account(1);
            account.Activate();
            account.Register();

            for (int i = 0; i < actionCount; i++)
            {
                account.TakeAction(_mockAction.Object);
            }
            _mockAccountRepository.Setup(repo => repo.GetAll()).Returns(new List<Account> { account });
            _mockAccountRepository.Setup(repo => repo.Get(1)).Returns(account);
            //ACT
            int count = service.GetAmountForActivity(activityLevel);
            //ASSERT
            return count;
        }
    }
}
